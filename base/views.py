# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import get_object_or_404, render
from base.serializers import *
from base.permissions import IsDriver, DriverNotSetted

from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework import generics, views, viewsets

from base.put_as_create import AllowPUTAsCreateMixin
# Create your views here.

def index(request):
    return render(request, 'index.html')

class UserViewSet(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UsersViewSet(generics.ListCreateAPIView):
    permission_classes = (AllowAny,)

    queryset = User.objects.all()
    serializer_class = UserSerializer


class EntriesViews(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = EntrySerializer

    def get_queryset(self):
        queryset = Entry.objects.all()
        term = self.request.query_params.get('filter', None)
        if term is not None:
            if term == '1':
                if self.request.user is not None and type(self.request.user != 'AnonymousUser'):
                    return queryset.filter(user=self.request.user)
            elif term == '2':
                if self.request.user.driver is not None:
                    return queryset.filter(driver=self.request.user.driver)
        else:
            return queryset

    def list(self, request):
        queryset = self.get_queryset()

        serializer = EntrySerializer(queryset, many=True)

        response = Response(serializer.data, headers={'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': '*'})
        return response

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class EntryView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer


class DriversView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class AddDriverToList(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated, IsDriver, DriverNotSetted,)
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer

    def perform_update(self, serializer):

        instance = serializer.save()
        if self.request.user.driver.id in serializer.data['drivers']:
            return
        instance.drivers.add(self.request.user.driver)
        instance.save()


class SetDriver(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated, IsDriver, )
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer

    def perform_update(self, serializer):
        instance = serializer.save()
        print self.request.data['driverID']
        if self.request.data['driverID'] is not None:
            driver = Driver.objects.get(pk=self.request.data['driverID'])

        if driver is not None:
            instance.driver = driver
            instance.save()


class IsDriver(views.APIView):

    def get(self, request, format=None):
        if Driver.objects.all().filter(user=request.user).exists():
            return Response(True)
        else:
            return Response(False)

