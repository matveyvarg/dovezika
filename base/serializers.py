from rest_framework import serializers

from base.models import *


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password', 'id']

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = '__all__'


class DriverSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    car = CarSerializer()

    class Meta:
        model = Driver
        fields = '__all__'
        extra_kwargs = {'user': {'read_only': True}}


class EntrySerializer(serializers.ModelSerializer):
    drivers = DriverSerializer(many=True)
    driver = DriverSerializer()
    class Meta:
        model = Entry
        fields = '__all__'
        extra_kwargs = {'user': {'read_only': True}, 'driver': {'read_only': True}}
