# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from base.models import Driver, Entry
# Register your models here.
admin.site.register(Driver)
admin.site.register(Entry)
