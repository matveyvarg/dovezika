# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Car(models.Model):
    brand = models.CharField(max_length=512)
    model = models.CharField(max_length=512)
    number = models.CharField(max_length=6)
    color = models.CharField(max_length=20)

class Driver(models.Model):

    user = models.OneToOneField(User, related_name='driver')
    age = models.PositiveSmallIntegerField()
    stage = models.PositiveSmallIntegerField()
    rate = models.PositiveSmallIntegerField(blank=True, null=True)
    car = models.OneToOneField(Car, related_name='driver', null=True, default=None)
    def __str__(self):
        return self.user.username


class Entry(models.Model):

    user = models.ForeignKey(User, related_name='entries')
    drivers = models.ManyToManyField(Driver, related_name='entries', blank=True, null=True)
    send_from = models.CharField(max_length=512)
    send_to = models.CharField(max_length=512)
    additional = models.TextField(default='', blank=True)
    weight = models.PositiveSmallIntegerField()
    x = models.PositiveSmallIntegerField(null=True)
    y = models.PositiveSmallIntegerField(null=True)
    z = models.PositiveSmallIntegerField(null=True)
    send = models.DateTimeField()
    arrive = models.DateTimeField()
    important = models.PositiveSmallIntegerField(choices=(('0', 0), ('1', 1), ('2', 2), ('3', 3), ('4', 4), ('5', 5)))
    addressee_name = models.CharField(max_length=128)
    phone = models.CharField(max_length=16)
    driver = models.ForeignKey(Driver, related_name='entry', null=True)
    phrase = models.CharField(max_length=128, default='', blank=True)
    def __str__(self):
        return str(self.pk)

class Cities(models.Model):

    name = models.CharField(max_length=64)
