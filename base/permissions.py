from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied

from base.models import Driver

class IsDriver(permissions.BasePermission):

    def has_permission(self, request, view):
        if Driver.objects.all().filter(user=request.user).exists():
            return True
        return False


class DriverNotSetted(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        drivers = list(obj.drivers.all())

        if(request.user.driver in drivers):
            return PermissionDenied({
                "message":"Already setted as driver"
            })
        return True
