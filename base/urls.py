from django.conf.urls import url, include

from rest_framework_jwt.views import obtain_jwt_token

from base import views

urlpatterns = [
    url(r'login/$', obtain_jwt_token),
    url(r'users/(?P<pk>\d+)/$', views.UserViewSet.as_view()),
    url(r'users/$', views.UsersViewSet.as_view()),
    url(r'entries/', include([
        url(r'^$', views.EntriesViews.as_view()),
        url(r'^(?P<id>\d+)/$', views.EntryView.as_view()),
        url(r'^(?P<pk>\d+)/add-driver/$', views.AddDriverToList.as_view()),
        url(r'^(?P<pk>\d+)/set-driver', views.SetDriver.as_view())
        # url(r'^/user/(?P<pk>\d+)/$', views.UserEntries.as_view())
    ])),
    url(r'drivers', include([
        url(r'$', views.DriversView.as_view())
    ])),
    url(r'is-driver', views.IsDriver.as_view())
]